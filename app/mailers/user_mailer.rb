class UserMailer < ApplicationMailer
  default return_path: 'info@syftet.com'
  default from: 'info@syftet.com'

  def send_campaign(recipient)
    @recipient = recipient
    @campaign = recipient.campaign
    # attachments["profile.pdf"] = mail_recipient.attachment.path
    #attachments["profile.pdf"] = open("#{mail_recipient.attachment.path}").read

    # mail(to: email, subject: @email_recipient.subject )
    mail(to: @recipient.email, subject: @campaign.subject, reply_to: "info@syftet.com", "recipient-variables" => {recipient: recipient.id})
  end

end
