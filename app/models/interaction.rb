class Interaction < ActiveRecord::Base
  TYPES = {
      open: 'open',
      click: 'click',
      reply: 'reply',
      comments: 'comments',
  }
  belongs_to :interactionable, polymorphic: true
end
