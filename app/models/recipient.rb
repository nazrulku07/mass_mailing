class Recipient < ActiveRecord::Base
  belongs_to :campaign
  has_many :interactions, as: :interactionable

  def opens
    interactions.where(interaction_type: Interaction::TYPES[:open])
  end

  def clicks
    interactions.where(interaction_type: Interaction::TYPES[:click])
  end
end
