class Campaign < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged
  validates_presence_of :title, :subject, :template
  has_many :recipients

  def publish(recipients)
    recipients.each do |email|
      begin
        recipient = self.recipients.create(email: email)
        begin
          UserMailer.send_campaign(recipient).deliver
        rescue Exception => ex
          p ex.message
          p "Error for sending email"
          recipient.update_attribute(:status, false)
        end
      rescue Exception => e
        p e.message
      end
    end
  end

  def rate
    (total_sent.to_f / total_recipient.to_f) * 100
  end

  def total_recipient
    recipients.count
  end

  def total_sent
    recipients.where(status: true).count
  end

  def total_failed
    recipients.where(status: false).count
  end

end
