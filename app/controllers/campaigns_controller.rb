class CampaignsController < ApplicationController
  protect_from_forgery with: :exception
  before_action :set_campaign, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    @campaigns = Campaign.all
  end

  def new
    @campaign = Campaign.new
  end

  def create
    @campaign = Campaign.new(campaign_params)
    respond_to do |format|
      format.html do
        if @campaign.save
          redirect_to @campaign
        else
          render :new
        end
      end
    end
  end

  def show

  end

  def edit

  end

  def update
    respond_to do |format|
      format.html do
        unless @campaign.update_attributes(campaign_params)
          flash[:error] = 'Unable to update campaign. Please try again!'
        end
        redirect_to @campaign
      end
    end
  end

  def destroy
    respond_to do |format|
      format.html do
        unless @campaign.delete
          flash[:error] = 'Unable to delete campaign. Please try again!'
        end
        redirect_to campaigns_path
      end
    end
  end

  def publish
    campaign = Campaign.friendly.find(params[:campaign_id])
    recipients = params['recipients']
    recipients = recipients.split(',').collect { |email| validate_recipient(email) }.compact
    campaign.publish(recipients)
    respond_to do |format|
      format.json {
        render json: {total: recipients.length}
      }
    end
  end

  def email_view
    @campaign = Campaign.friendly.find(params[:campaign_id])
    render layout: 'mailer'
  end

  private

  def set_campaign
    @campaign = Campaign.friendly.find(params[:id])
    unless @campaign.present?
      render action: 404
    end
  end

  def campaign_params
    params.require(:campaign).permit!
  end

  def validate_recipient(email)
    regex = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
    email = email.strip
    email =~ regex ? email : nil
  end

end
