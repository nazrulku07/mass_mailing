class Webhook::EmailCampaignController < ApplicationController
  before_action :set_recipient

  def delivered
    render nothing: true
  end

  def opened
    if @recipient.present?
      @recipient.update_attributes(open: recipient.open + 1)
    end
    render nothing: true
  end

  def clicked
    render nothing: true
  end

  def bounced
    render nothing: true
  end

  def spamed
    render nothing: true
  end

  private

  def set_recipient
    headers = params['message-headers']
    begin
      headers = JSON.parse(headers)
      custom_var = headers.last
      if custom_var.present? && custom_var.kind_of?(Array)
        recipient = custom_var.last
        recipient = JSON.parse(recipient.gsub('=>', ':'))
        if recipient.present?
          @recipient = Recipient.find_by_id(recipient['recipient'])
        end
      end
    rescue Exception => ex
      p ex.message
      @recipient = nil
    end
  end

end
