class HomeController < ApplicationController
  protect_from_forgery with: :exception
  before_action :authenticate_user!, only: [:index]

  def index
    #@mail = Mail.last
    #p @mail
    # text = ActionView::Base.full_sanitizer.sanitize(@mail.body.raw_source)
    # @email_content = @mail.body.decoded.split('wrote:').first
    # #@email_content = text.split('wrote:').first
    # @text = @email_content.split('quoted-printable').last
    # p mail.header
    # p "Message: #{mail.body.decoded}"
    @campaign = Campaign.all
    @recipients = Recipient.all
  end

  def email_open
    render nothing: true
  end

end
