class SocialCampaignsController < ApplicationController
  protect_from_forgery with: :exception

  def index
    @social_campaigns = SocialCampaign.all
  end

  def new
    @social_campaign = SocialCampaign.new
  end

  def create
    @social_campaign = SocialCampaign.new(campaign_params)
    respond_to do |format|
      format.html do
        if @social_campaign.save
          redirect_to @social_campaign
        else
          render :new
        end
      end
    end
  end

  def show
    @social_campaign = SocialCampaign.friendly.find(params[:id])
  end


  def edit
    @social_campaign = SocialCampaign.friendly.find(params[:id])
  end

  def update
    @social_campaign = SocialCampaign.friendly.find(params[:id])
    respond_to do |format|
      format.html do
        unless @social_campaign.update_attributes(campaign_params)
          flash[:error] = 'Unable to update campaign. Please try again!'
        end
        redirect_to @social_campaign
      end
    end
  end


  def destroy
    @social_campaign = SocialCampaign.friendly.find(params[:id])
    respond_to do |format|
      format.html do
        unless @social_campaign.delete
          flash[:error] = 'Unable to delete campaign. Please try again!'
        end
        redirect_to social_campaigns_path
      end
    end
  end

  def share
    @social_campaign = SocialCampaign.friendly.find(params[:id])
  end


  private

  def campaign_params
    params.require(:social_campaign).permit!
  end

end
