class RecipientsController < ApplicationController
  protect_from_forgery with: :exception
  before_action :authenticate_user!, except: [:open]

  def index
    campaign = Campaign.friendly.find(params[:campaign_id])
    @recipients = campaign.recipients
  end

  def show

  end

  def open
    recipient = Recipient.find_by_id(params[:recipient_id])
    recipient.interactions.create(interaction_type: Interaction::TYPES[:open])
    render nothing: true
  end
end
