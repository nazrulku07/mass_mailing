require 'httparty'
require 'nokogiri'
require 'uri'
require 'social_shares'
module ApplicationHelper
  def active_collapse(menu)
    menu.each do |m|
      return 'in' if m == controller_name
    end
  end

  def get_fb_share(link)
    SocialShares.facebook link
  end

  def get_linkedIn_share(link)
    SocialShares.linkedin link
  end

  def google_plus_share(link)
    SocialShares.google link
  end

  def get_twitter_share(link)
    page = HTTParty.get("http://cdn.api.twitter.com/1/urls/count.json?url=#{link}")
    page['share']['share_count']
  end
end
