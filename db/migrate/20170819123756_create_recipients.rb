class CreateRecipients < ActiveRecord::Migration
  def change
    create_table :recipients do |t|
      t.integer :campaign_id
      t.string :email
      t.integer :open, default: 0
      t.integer :click, default: 0

      t.timestamps null: false
    end
  end
end
