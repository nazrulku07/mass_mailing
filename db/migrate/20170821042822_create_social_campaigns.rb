class CreateSocialCampaigns < ActiveRecord::Migration
  def change
    create_table :social_campaigns do |t|
      t.string :link
      t.string :image
      t.text :description

      t.timestamps null: false
    end
  end
end
