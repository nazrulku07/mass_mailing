class AddSlugFieldToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :slug, :string
    add_column :social_campaigns, :slug, :string
  end
end
