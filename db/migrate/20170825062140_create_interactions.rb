class CreateInteractions < ActiveRecord::Migration
  def change
    create_table :interactions do |t|
      t.integer :interactionable_id
      t.string :interactionable_type
      t.string :interaction_type

      t.timestamps null: false
    end
  end
end
