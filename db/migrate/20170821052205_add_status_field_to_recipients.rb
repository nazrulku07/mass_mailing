class AddStatusFieldToRecipients < ActiveRecord::Migration
  def change
    add_column :recipients, :status, :boolean, default: true
  end
end
