class AddTitleFieldToSocialCampaigns < ActiveRecord::Migration
  def change
    add_column :social_campaigns, :title, :string
  end
end
