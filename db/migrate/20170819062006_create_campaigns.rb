class CreateCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
      t.string :title
      t.string :subject
      t.text :template
      t.string :attachment

      t.timestamps null: false
    end
  end
end
