Rails.application.routes.draw do
  get 'mail_stores/template'

  #devise_for :users, controllers: { sessions: 'users/sessions' }
  devise_for :users, controllers: {
                       sessions: 'users/sessions',
                       passwords: 'users/passwords',
                       registrations: 'users/registrations',
                       unlocks: 'users/unlocks',
                       confirmations: 'users/confirmations',
                       invitations: 'users/invitations'
                   }
  get 'home/index'
  get :email_open, to: 'home#email_open'
  #  devise_for :users, path: 'auth', path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', unlock: 'unblock', registration: 'register', sign_up: 'cmon_let_me_in' }


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  resources :mail_stores
  resources :campaigns do
    resources :recipients do
      get :open
    end
    get :email_view
    post :publish
  end

  namespace :webhook do
    post :delivered, to: 'email_campaign#delivered'
    post :opened, to: 'email_campaign#opened'
    post :clicked, to: 'email_campaign#clicked'
    post :bounced, to: 'email_campaign#bounced'
    post :spamed, to: 'email_campaign#spamed'
  end

  resources :social_campaigns do
    get :share, on: :member
  end
  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
